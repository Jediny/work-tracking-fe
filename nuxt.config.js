require('dotenv').config();
const pkg = require('./package');

module.exports = {
    mode: 'universal',

    head: {
        title: pkg.name,
        meta: [{
            charset: 'utf-8'
        }, {
            name: 'viewport',
            content: 'width=device-width, initial-scale=1'
        }, {
            hid: 'description',
            name: 'description',
            content: pkg.description
        }],
        link: [{
            href: 'https://fonts.googleapis.com/icon?family=Material+Icons',
            rel: 'stylesheet'
        }, {
            href: 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css',
            integrity: 'sha256-l85OmPOjvil/SOvVt3HnSSjzF1TUMyT9eV0c2BzEGzU=',
            crossorigin: 'anonymous',
            rel: 'stylesheet'
        }, {
            href: 'https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css',
            rel: 'stylesheet'
        }, {
            rel: 'icon',
            type: 'image/x-icon',
            href: '/favicon.ico'
        }],
        script: [{
            src: 'https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js'
        }]
    },
    loading: {
        color: '#fff'
    },
    css: [
        '~/assets/style/bootstrap-grid.scss',
        '~/assets/style/custom.scss',
        '~/assets/style/u-show-grid.scss'
    ],
    plugins: ['~/plugins/axios', '~/plugins/textarea'],
    modules: [
        '@nuxtjs/dotenv',
        '@nuxtjs/axios'
        // '@nuxtjs/auth'
    ],
    axios: {
        baseURL: `${process.env.BACKEND_URL}${process.env.BACKEND_PATH}`,
        credentials: false
    },
    auth: {},
    build: {
        extractCSS: true,
        extend(config, ctx) {
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                });
            }
        }
    }
};
