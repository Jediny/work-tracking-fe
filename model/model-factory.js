import projectModel from './project-model';

export default (axios) => {
    return {
        project: projectModel(axios)
    };
};
