export default (axios) => {
    return {
        async getProjects() {
            return (await axios.$get('/projects', {
                headers: {
                    'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                }
            })).projects;
        },
        async getProject(id) {
            return (await axios.$get(`/projects/${id}`, {
                headers: {
                    'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                }
            })).project;
        },
        createProject(id, projectData) {
            return axios.put(`/projects/${id}`, {
                project: projectData
            }, {
                headers: {
                    'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                }
            });
        },
        updateProject(id, projectData) {
            return axios.put(`/projects/${id}`, {
                project: projectData
            }, {
                headers: {
                    'Authorization': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
                }
            });
        }
    };
};