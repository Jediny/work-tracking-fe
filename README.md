# work-tracking

smazat databázi a znovu ji zmigrovat.

```bash
docker-compose down --volumes
make build
make up
```

A v dalším shellu

```bash
make migrate
make default-user
```

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
