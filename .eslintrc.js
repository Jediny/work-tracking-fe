module.exports = {
    root: true,
    env: {
        browser: true,
        commonjs: true,
        es6: true,
        node: true
    },
    parserOptions: {
        parser: 'babel-eslint',
        ecmaVersion: 2017,
        sourceType: 'module'
    },
    extends: [
        'plugin:vue/essential',
        'eslint:recommended'
    ],
    plugins: [
        'vue',
        'html'
    ],
    rules: {
        eqeqeq: [2, 'smart'],
        indent: [2, 4, {
            SwitchCase: 1
        }],
        'no-console': 0,
        'linebreak-style': ['error', 'unix'],
        quotes: ['error', 'single'],
        semi: ['error', 'always']
    }
}