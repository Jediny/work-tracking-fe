import modelFactory from '~/model/model-factory';

export default function ({
    $axios
}, inject) {
    const api = modelFactory($axios);
    inject('api', api);
}